import asyncio
import logging

from app.main import main

if __name__ == '__main__':
    try:
        logging.info("Starting bot")
        asyncio.run(main())
    except (KeyboardInterrupt, SystemExit):
        logging.error("Bot stopped!")

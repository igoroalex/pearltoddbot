from pydantic import BaseSettings


class Settings(BaseSettings):
    APP_NAME: str = "PearlToddBot"
    BOT_TOKEN: str
    URL_API: str
    ADMIN_TELEGRAM_ID: str
    SERVICE_TELEGRAM_ID: str

    SECRET_KEY: str
    ALGORITHM: str

    class Config:
        env_file = '.env'


settings = Settings()

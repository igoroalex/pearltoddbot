import calendar
from datetime import date

from aiogram.utils.callback_data import CallbackData

from app.keyboards.calendar import BaseCalendar
from app.utils.response_api import ResponseAPI

master_calendar_callback = CallbackData('master_calendar', 'act', 'date')
show_master_date_callback = CallbackData('show_master_date', 'act', 'date')


class MasterCalendar(BaseCalendar):
    def __init__(self, user_id: int, current_date: date = None):
        super().__init__()

        res = ResponseAPI('/api/work_dates/', user_id).get()
        work_dates = [date.fromisoformat(i['date']) for i in res.json()]

        res = ResponseAPI('/api/bookings/dates/', user_id).get()
        booking_dates = [date.fromisoformat(x['start_datetime']) for x in res.json()]

        self.callback = master_calendar_callback
        self.work_dates = work_dates
        self.booking_dates = booking_dates
        self.current_date: date = current_date if current_date else date.today()

    def get_header(self) -> str:
        return f"🔄 {calendar.month_name[self.current_date.month]} {str(self.current_date.year)} 🔄"

    def refresh(self) -> str:
        return self.callback.new('REFRESH', self.current_date)

    def get_text_callback_data_day(self, day_date: date) -> tuple:
        smile = ""
        callback = self.ignore()
        if day_date in self.booking_dates:
            smile = "⏰"
            callback = show_master_date_callback.new('SHOW_MASTER_DATE', day_date)
        elif day_date in self.work_dates:
            smile = "🤖"
            callback = show_master_date_callback.new('SHOW_MASTER_DATE', day_date)

        text_button = f"{smile}{day_date.day}"
        if day_date < date.today():
            text_button = f"--"

        return text_button, callback

from datetime import date

from aiogram.utils.callback_data import CallbackData

from app.keyboards.calendar import BaseCalendar
from app.schemas.work_date import WorkDate

work_date_calendar_callback = CallbackData('work_date_calendar', 'act', 'date')


class WorkDateCalendar(BaseCalendar):
    def __init__(self, work_dates: list, current_date: date = None):
        super().__init__()
        self.callback = work_date_calendar_callback
        self.work_dates = work_dates
        self.current_date: date = current_date if current_date else date.today()

    def get_text_callback_data_day(self, day_date: date) -> tuple:
        cur_date = WorkDate(date=day_date)
        smile = "🤖" if cur_date in self.work_dates else ""
        text_button = f"{cur_date.date.day}{smile}"
        callback = (
            self.callback.new('DEL_DATE', cur_date.date)
            if cur_date in self.work_dates
            else self.callback.new('ADD_DATE', cur_date.date)
        )
        if cur_date.date < date.today():
            text_button = f"--"
            callback = self.ignore()

        return text_button, callback

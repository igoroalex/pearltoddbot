from datetime import datetime, timezone

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

time_callback = CallbackData('time_callback', 'time', sep=';')


async def times_keyboard() -> InlineKeyboardMarkup:
    inline_kb = InlineKeyboardMarkup(row_width=4)

    for d in range(6 * 60, 22 * 60, 30):
        t = datetime.fromtimestamp(d * 60, tz=timezone.utc).strftime('%H:%M')
        inline_kb.insert(InlineKeyboardButton(t, callback_data=time_callback.new(t)))
    return inline_kb

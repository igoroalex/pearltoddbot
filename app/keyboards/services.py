from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

from app.schemas.service import Service
from app.utils.response_api import ResponseAPI

booking_service_callback = CallbackData('booking_service_callback', 'service_id')
service_callback = CallbackData('service_callback', 'id')
service_common_callback = CallbackData('service_common_callback', 'act')


async def services_keyboard(msg_from_id: int) -> InlineKeyboardMarkup:
    res = ResponseAPI('/api/services/', msg_from_id).get()
    services = [Service(**i) for i in res.json()]

    inline_kb = InlineKeyboardMarkup(row_width=2)

    for s in services:
        inline_kb.insert(InlineKeyboardButton(s.short_name(), callback_data=service_callback.new(s.id)))

    inline_kb.row()
    inline_kb.insert(InlineKeyboardButton("Додати послугу", callback_data=service_common_callback.new('ADD_SERVICE')))
    inline_kb.row()
    inline_kb.insert(InlineKeyboardButton("Відміна", callback_data=service_common_callback.new('CANCEL')))
    return inline_kb


async def booking_service_keyboard(msg_from_id: int) -> InlineKeyboardMarkup:
    res = ResponseAPI('/api/services/', msg_from_id).get()
    services = [Service(**i) for i in res.json()]

    inline_kb = InlineKeyboardMarkup(row_width=2)

    for s in services:
        inline_kb.insert(InlineKeyboardButton(s.short_name(), callback_data=booking_service_callback.new(s.id)))

    return inline_kb

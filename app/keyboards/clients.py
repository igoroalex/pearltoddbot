from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

from app.schemas.client import Client
from app.utils.response_api import ResponseAPI

booking_client_callback = CallbackData('booking_client_callback', 'client_id')
client_common_callback = CallbackData('client_common_callback', 'act')


async def clients_keyboard(filters, msg) -> InlineKeyboardMarkup:
    res = ResponseAPI('/api/clients/', msg.chat.id).get(filters)

    if res.status_code == 404:
        await msg.answer(f"кліентів не знайдено")

    inline_kb = InlineKeyboardMarkup(row_width=1)
    if res.status_code == 200:
        clients = [Client(**i) for i in res.json()]
        for s in clients:
            inline_kb.insert(InlineKeyboardButton(str(s), callback_data=booking_client_callback.new(s.id)))
    inline_kb.row()
    inline_kb.insert(InlineKeyboardButton("Додати кліента", callback_data=client_common_callback.new('ADD_CLIENT')))
    inline_kb.row()
    inline_kb.insert(InlineKeyboardButton("Відміна", callback_data=client_common_callback.new('CANCEL')))
    return inline_kb

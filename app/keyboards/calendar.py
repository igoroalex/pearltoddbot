import calendar
from datetime import date

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData


class BaseCalendar:
    def __init__(self):
        self.callback = CallbackData('base_calendar', 'act', 'date')
        self.current_date: date = date.today()

    def insert_date(self, inline_kb: InlineKeyboardMarkup, day: int):
        day_date = date(self.current_date.year, self.current_date.month, day)

        text_button, callback_data = self.get_text_callback_data_day(day_date)
        inline_kb.insert(InlineKeyboardButton(text_button, callback_data=callback_data))

    def get_text_callback_data_day(self, day_date: date) -> tuple:
        return f"{day_date.day}", self.ignore()

    def ignore(self) -> str:
        return self.callback.new('IGNORE', self.current_date)

    def prev_month(self) -> str:
        return self.callback.new('PREV-MONTH', self.current_date)

    def next_month(self) -> str:
        return self.callback.new('NEXT-MONTH', self.current_date)

    def cancel(self) -> str:
        return self.callback.new('CANCEL', self.current_date)

    def refresh(self) -> str:
        return self.ignore()

    def get_header(self) -> str:
        return self.current_date.strftime('%B %Y')

    async def start_calendar(self) -> InlineKeyboardMarkup:
        inline_kb = InlineKeyboardMarkup(row_width=7)

        # First row - Month and Year
        inline_kb.row()
        inline_kb.insert(InlineKeyboardButton(self.get_header(), callback_data=self.refresh()))
        # Second row - Week Days
        inline_kb.row()
        for x in ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]:
            inline_kb.insert(InlineKeyboardButton(x, callback_data=self.ignore()))

        # Calendar rows - Days of month
        month_calendar = calendar.monthcalendar(self.current_date.year, self.current_date.month)
        for week in month_calendar:
            inline_kb.row()
            for day in week:
                if day == 0:
                    inline_kb.insert(InlineKeyboardButton(" ", callback_data=self.ignore()))
                    continue
                self.insert_date(inline_kb, day)

        # Last row - Buttons
        inline_kb.row()
        inline_kb.insert(InlineKeyboardButton("<", callback_data=self.prev_month()))
        inline_kb.insert(InlineKeyboardButton(" cancel ", callback_data=self.cancel()))
        inline_kb.insert(InlineKeyboardButton(">", callback_data=self.next_month()))

        return inline_kb

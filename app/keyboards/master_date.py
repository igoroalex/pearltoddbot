from datetime import date

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData

from app.handlers.master_calendar import master_calendar_callback
from app.keyboards.master_calendar import show_master_date_callback
from app.schemas.booking import ZeroBooking, ServiceBooking
from app.utils.response_api import ResponseAPI

master_date_callback = CallbackData('booking_date_callback', 'act', 'date', sep=';')
master_datetime_callback = CallbackData('master_datetime_callback', 'act', 'datetime', sep=';')
booking_callback = CallbackData('booking_callback', 'act', 'id')


async def master_date_keyboard(user_id: int, current_date: date) -> InlineKeyboardMarkup:
    inline_kb = InlineKeyboardMarkup(row_width=5)

    res = ResponseAPI('/api/bookings/date/', user_id).get({'current_date': current_date})

    bookings = []
    if res.status_code == 200:
        bookings = [ZeroBooking(**i) if i.get('zero') else ServiceBooking(**i) for i in res.json()]

    inline_kb.row()
    header = f"🔄 {current_date.strftime('%B %d %A')} 🔄"

    inline_kb.insert(
        InlineKeyboardButton(header, callback_data=show_master_date_callback.new('SHOW_MASTER_DATE', current_date))
    )

    inline_kb.row()
    for b in bookings:
        if b.zero:
            inline_kb.insert(
                InlineKeyboardButton(
                    f"{b.start_datetime.strftime('%H:%M')}",
                    callback_data=master_datetime_callback.new('ADD_BOOKING', b.start_datetime),
                )
            )
        else:
            inline_kb.row()
            inline_kb.insert(
                InlineKeyboardButton(b.master_date(), callback_data=booking_callback.new('SHOW_BOOKING', b.id))
            )
            inline_kb.row()

    inline_kb.row()
    inline_kb.insert(
        InlineKeyboardButton("<", callback_data=master_date_callback.new('PREV_MASTER_DATE', current_date))
    )
    inline_kb.insert(
        InlineKeyboardButton(" календар ", callback_data=master_calendar_callback.new('REFRESH', current_date))
    )
    inline_kb.insert(
        InlineKeyboardButton(">", callback_data=master_date_callback.new('NEXT_MASTER_DATE', current_date))
    )
    return inline_kb

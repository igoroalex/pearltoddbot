from aiogram import types
from aiogram.contrib.middlewares.i18n import I18nMiddleware


async def get_lang(user_id):
    return None


class ACLMiddleware(I18nMiddleware):
    async def get_user_locale(self, action: str):
        user = types.User.get_current()
        return user.locale

# def setup_middleware(dp):
#     i18n = ACLMiddleware(I18_)



from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.types import Message, CallbackQuery
from aiogram.utils.callback_data import CallbackData

from ..bot import dp
from ..keyboards.services import services_keyboard, service_common_callback, service_callback
from ..schemas.service import ServiceCreate, Service
from ..state.service import CreateServiceFSM
from ..utils.response_api import ResponseAPI


@dp.message_handler(Command("add_service"))
async def create_service_start(msg: Message):
    services = ResponseAPI('/api/services/', msg.from_id).get()

    if services.status_code == 404:
        await msg.answer(f"послуг немає")
        await CreateServiceFSM.start.set()
        await CreateServiceFSM.next()
        await msg.answer(f"додаєм послугу, введить назву послуги чи введить . щоб припинити")
        return

    await msg.answer(f"вже є послуги", reply_markup=await services_keyboard(msg.from_id))


@dp.callback_query_handler(service_common_callback.filter())
async def service_add_cancel(callback_query: CallbackQuery, callback_data: CallbackData):
    msg = callback_query.message
    act = callback_data['act']

    match act:
        case 'CANCEL':
            await msg.answer("💡 добрий список послуг")

        case 'ADD_SERVICE':
            await CreateServiceFSM.start.set()
            await CreateServiceFSM.next()
            await msg.answer(f"додаєм послугу, введить назву послуги чи введить . щоб припинити")


@dp.callback_query_handler(service_callback.filter())
async def patch_service(callback_query: CallbackQuery, callback_data: CallbackData, state: FSMContext):
    msg = callback_query.message
    await msg.delete_reply_markup()

    await state.update_data({'id': callback_data['id']})

    await CreateServiceFSM.start.set()
    await CreateServiceFSM.next()
    await msg.answer(f"змінюємо послугу, введить назву послуги чи введить . щоб припинити")


@dp.message_handler(state=CreateServiceFSM.name)
async def create_service_name(msg: Message, state: FSMContext):

    await state.update_data({'name': msg.text.replace(':', '_')})

    if msg.text == '.':
        await state.finish()
        await msg.answer("💡 добрий список послуг")
        return
    await CreateServiceFSM.next()
    await msg.answer(f"ціна послуги")


@dp.message_handler(state=CreateServiceFSM.price)
async def create_service_price(msg: Message, state: FSMContext):

    price = msg.text

    if price == '.':
        await state.finish()
        await msg.answer("💡 добрий список послуг")
        return

    if not price.isdigit():
        await msg.answer(f"ціна послуги наприклад: 250, якщо введете 0 - послуга не буде відображатися")
        await CreateServiceFSM.price.set()
        return

    await state.update_data({'price': price})
    await msg.answer(f"час витраченний на послугу у хвилинах")
    await CreateServiceFSM.next()


@dp.message_handler(state=CreateServiceFSM.duration)
async def create_service_duration(msg: Message, state: FSMContext):

    duration = msg.text

    if duration == '.':
        await state.finish()
        await msg.answer("💡 добрий список послуг")
        return

    if not duration.isdigit():
        await msg.answer(f"треба вказувати час в хвилинах: 45")
        await CreateServiceFSM.duration.set()
        return

    await state.update_data({'duration': duration})
    await msg.answer(f"підготовка та відпочинок після послуги в хвилинах")
    await CreateServiceFSM.next()


@dp.message_handler(state=CreateServiceFSM.rest_time)
async def create_service_rest_time(msg: Message, state: FSMContext):

    rest_time = msg.text

    if rest_time == '.':
        await state.finish()
        await msg.answer("💡 добрий список послуг")
        return

    if not rest_time.isdigit():
        await msg.answer(f"треба вказувати час в хвилинах: 5")
        await CreateServiceFSM.rest_time.set()
        return

    await state.update_data({'rest_time': rest_time})
    await msg.answer(
        f"більш детальний опис для кліента, наприклад: не мити голову за день, якщо точка . - то пусто"
    )
    await CreateServiceFSM.next()


@dp.message_handler(state=CreateServiceFSM.description)
async def create_service_description(msg: Message, state: FSMContext):

    description = "" if msg.text == "." else msg.text
    await state.update_data({'description': description})

    await msg.answer(f"коментар для себе, типу: підготувати фарбу та шампунь, якщо точка . - то пусто")
    await CreateServiceFSM.next()


@dp.message_handler(state=CreateServiceFSM.comment)
async def create_service_comment(msg: Message, state: FSMContext):

    comment = "" if msg.text == "." else msg.text
    await state.update_data({'comment': comment})
    data = await state.get_data()

    if data.get('id'):
        service = Service(
            id=data['id'],
            name=data['name'],
            price=data['price'],
            duration=data['duration'],
            rest_time=data['rest_time'],
            description=data['description'],
            comment=data['comment'],
        )
        res = ResponseAPI(f'/api/services/{data["id"]}', msg.from_id).patch(service)
    else:
        service = ServiceCreate(
            user_id=msg.from_id,
            name=data['name'],
            price=data['price'],
            duration=data['duration'],
            rest_time=data['rest_time'],
            description=data['description'],
            comment=data['comment'],
        )
        res = ResponseAPI('/api/services/', msg.from_id).post(service)

    service = Service(**res.json())

    await msg.answer(str(service))
    await state.finish()

    await create_service_start(msg)

import calendar
from datetime import date

from aiogram.dispatcher.filters import Command
from aiogram.types import CallbackQuery, Message
from aiogram.utils.callback_data import CallbackData

from ..bot import dp
from ..keyboards.workdates import WorkDateCalendar, work_date_calendar_callback
from ..schemas.schedule import Schedule
from ..schemas.work_date import WorkDateCreate, WorkDate
from ..utils.response_api import ResponseAPI


@dp.message_handler(Command("add_work_date"))
async def create_work_date_start(msg: Message):
    res = ResponseAPI('/api/schedules/', msg.from_id).get()

    if res.status_code == 404:
        await msg.answer(f"зараз створимо график за яким ви працюете /add_schedule")
        return
    elif res.status_code != 200:
        await msg.answer(f"Ми не знайомі ((( тицяєм /start")
        return

    schedules = [Schedule(**i) for i in res.json()]
    await msg.answer(f"Ваш графік {schedules[0].name}")

    res = ResponseAPI('/api/work_dates/', msg.from_id).get()
    work_dates = [WorkDate(**x) for x in res.json()]
    await msg.answer("коли я збираюсь працювати? ", reply_markup=await WorkDateCalendar(work_dates).start_calendar())


@dp.callback_query_handler(work_date_calendar_callback.filter())
async def process_work_date_calendar(query: CallbackQuery, callback_data: CallbackData):
    msg = query.message
    act = callback_data['act']
    cur_date: date = date.fromisoformat(callback_data['date'])

    match act:
        case 'ADD_DATE':
            work_date = WorkDateCreate(user_id=msg.chat.id, date=cur_date)
            res = ResponseAPI('/api/work_dates/', msg.chat.id).post(work_date)
            work_date = WorkDate(**res.json())
            await msg.answer(f"⏰ {work_date.date.strftime('%d.%m.%Y %A')} 🤖")

            res = ResponseAPI('/api/work_dates/', msg.chat.id).get()
            work_dates = [WorkDate(**x) for x in res.json()]
            await query.message.edit_reply_markup(await WorkDateCalendar(work_dates, cur_date).start_calendar())

        case 'DEL_DATE':
            work_date = WorkDateCreate(user_id=msg.chat.id, date=cur_date)
            res = ResponseAPI('/api/work_dates/', msg.chat.id).delete(work_date)
            work_date = WorkDate(**res.json())
            await msg.answer(f" 🧘 {work_date.date.strftime('%d.%m.%Y %A')} 🥐")

            res = ResponseAPI('/api/work_dates/', msg.chat.id).get()
            work_dates = [WorkDate(**x) for x in res.json()]
            await query.message.edit_reply_markup(await WorkDateCalendar(work_dates, cur_date).start_calendar())

        case 'PREV-MONTH':
            y, m = calendar._prevmonth(cur_date.year, cur_date.month)
            new_date = date(y, m, 1)
            res = ResponseAPI('/api/work_dates/', msg.chat.id).get()
            work_dates = [WorkDate(**x) for x in res.json()]
            await query.message.edit_reply_markup(await WorkDateCalendar(work_dates, new_date).start_calendar())

        case 'NEXT-MONTH':
            y, m = calendar._nextmonth(cur_date.year, cur_date.month)
            new_date = date(y, m, 1)
            res = ResponseAPI('/api/work_dates/', msg.chat.id).get()
            work_dates = [WorkDate(**x) for x in res.json()]
            await query.message.edit_reply_markup(await WorkDateCalendar(work_dates, new_date).start_calendar())

        case 'CANCEL':
            await query.message.delete_reply_markup()
            await query.answer(cache_time=2)
            await msg.answer("🧘 відпочивати теж потрібно 🥐")

        case 'IGNORE':
            await query.answer(cache_time=2)
            await msg.answer("оберіть дату, чи відміна")

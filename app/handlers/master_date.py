from datetime import date, timedelta

from aiogram.types import CallbackQuery
from aiogram.utils.callback_data import CallbackData

from ..bot import dp
from ..utils.response_api import ResponseAPI
from ..keyboards.master_calendar import show_master_date_callback
from ..keyboards.master_date import master_date_keyboard, booking_callback, master_date_callback
from ..schemas.booking import ServiceBooking


@dp.callback_query_handler(show_master_date_callback.filter())
async def process_master_calendar(query: CallbackQuery, callback_data: CallbackData):
    msg = query.message
    act = callback_data['act']
    current_date = date.fromisoformat(callback_data['date'])

    match act:
        case 'SHOW_MASTER_DATE':
            await query.message.delete_reply_markup()
            await msg.answer(f"🗓 Дивимось наш графік 🗓", reply_markup=await master_date_keyboard(msg.chat.id,
                                                                                                 current_date))


@dp.callback_query_handler(booking_callback.filter())
async def process_master_date(query: CallbackQuery, callback_data: CallbackData):
    msg = query.message
    act = callback_data['act']
    booking_id = callback_data['id']

    match act:
        case 'SHOW_BOOKING':
            res = ResponseAPI('/api/bookings/booking/', msg.from_id).get({'booking_id': booking_id})
            booking = ServiceBooking(**res.json())
            await msg.answer(str(booking))


@dp.callback_query_handler(master_date_callback.filter())
async def process_master_date(query: CallbackQuery, callback_data: CallbackData):
    msg = query.message
    act = callback_data['act']
    current_date = date.fromisoformat(callback_data['date'])

    match act:
        case 'PREV_MASTER_DATE':
            current_date = current_date - timedelta(days=1)
            await query.message.edit_reply_markup(
                await master_date_keyboard(msg.chat.id, current_date))

        case 'NEXT_MASTER_DATE':
            current_date = current_date + timedelta(days=1)
            await query.message.edit_reply_markup(
                await master_date_keyboard(msg.chat.id, current_date))

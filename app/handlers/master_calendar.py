import calendar
from datetime import date

from aiogram.dispatcher.filters import Command, Text
from aiogram.types import Message, CallbackQuery
from aiogram.utils.callback_data import CallbackData

from ..bot import dp
from ..keyboards.master_calendar import MasterCalendar, master_calendar_callback
from ..schemas.schedule import Schedule
from ..utils.response_api import ResponseAPI


@dp.message_handler(Text(equals="мій календар"))
@dp.message_handler(Command("show_master_calendar"))
async def master_calendar_start(msg: Message):
    res = ResponseAPI('/api/schedules/', msg.from_id).get()

    if res.status_code == 404:
        await msg.answer(f"зараз створимо график за яким ви працюете /add_schedule")
        return
    elif res.status_code != 200:
        await msg.answer(f"Ми не знайомі ((( тицяєм /start")
        return

    schedules = [Schedule(**i) for i in res.json()]

    await msg.answer(f"Ваш графік {schedules[0].name}")
    await msg.answer(f"⏰ - робочі дати з бронюваннями 🤖 - робочі дати")
    await msg.answer("оберіть дату щоб подивитись детальну інфу ",
                     reply_markup=await MasterCalendar(msg.from_id).start_calendar())


@dp.callback_query_handler(master_calendar_callback.filter())
async def process_master_calendar(query: CallbackQuery, callback_data: CallbackData):
    msg = query.message
    act = callback_data['act']
    work_date = date.fromisoformat(callback_data['date'])

    match act:
        case 'REFRESH':
            await query.message.edit_reply_markup(
                await MasterCalendar(msg.chat.id, work_date).start_calendar())

        case 'PREV-MONTH':
            y, m = calendar._prevmonth(work_date.year, work_date.month)
            new_date = date(y, m, work_date.day)

            await query.message.edit_reply_markup(
                await MasterCalendar(msg.chat.id, new_date).start_calendar())

        case 'NEXT-MONTH':
            y, m = calendar._nextmonth(work_date.year, work_date.month)
            new_date = date(y, m, work_date.day)

            await query.message.edit_reply_markup(
                await MasterCalendar(msg.chat.id, new_date).start_calendar())

        case 'CANCEL':
            await query.message.delete_reply_markup()
            # TODO: what is cache_time=2
            await query.answer(cache_time=2)
            await msg.answer("🚴‍♀️ побачимось пізніше ☕️")

        case 'IGNORE':
            await query.answer(cache_time=2)
            await msg.answer("а що ми тут бажаемо побачити??? 😄")

from aiogram.dispatcher.filters import Command
from aiogram.types import Message

from ..bot import dp
from ..keyboards.common import common_keyboard
from ..schemas.master import MasterCreate
from ..utils.response_api import ResponseAPI


@dp.message_handler(Command("start"))
async def command_start(msg: Message):
    await msg.answer("привіт, что тут робимо і для чого цей бот")

    res = ResponseAPI('/api/masters/master', msg.from_id).get()
    if res.status_code == 404:
        master = MasterCreate(
            full_name=msg.from_user.full_name, telegram_id=msg.from_id, username=msg.from_user.username
        )

        ResponseAPI('/api/masters/', msg.from_id).post(master)
        await msg.answer(
            f"регистрация прошла успешно, зайдите в меню та налаштуйте роботу бота", reply_markup=common_keyboard()
        )

    await command_help(msg)


@dp.message_handler(Command("help"))
async def command_help(msg: Message):
    await msg.answer(
        f"зайдите в меню та налаштуйте роботу бота чи можна за командами\n"
        f"1. <b>додати</b> адресу /add_address\n"
        f"2. додати графік роботи /add_schedule\n"
        f"3. додати розклад по дням /add_work_date\n"
        f"4. додати прайс послуг /add_service\n"
        f"5. мій календар /show_master_calendar",
        reply_markup=common_keyboard(),
        parse_mode='HTML',
    )

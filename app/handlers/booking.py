from aiogram.dispatcher import FSMContext
from aiogram.types import Message, CallbackQuery
from aiogram.utils.callback_data import CallbackData

from ..bot import dp
from ..keyboards.clients import clients_keyboard, client_common_callback, booking_client_callback
from ..keyboards.master_date import master_date_keyboard, master_datetime_callback
from ..keyboards.services import booking_service_keyboard, booking_service_callback
from ..schemas.booking import BookingCreate, ServiceBooking
from ..schemas.client import ClientCreate, Client
from ..state.booking import CreateBookingFSM
from ..utils.response_api import ResponseAPI


@dp.callback_query_handler(master_datetime_callback.filter())
async def process_master_datetime(query: CallbackQuery, callback_data: CallbackData, state: FSMContext):
    msg = query.message
    act = callback_data['act']

    match act:
        case 'ADD_BOOKING':
            await msg.delete_reply_markup()

            res = ResponseAPI('/api/services/', msg.chat.id).get()
            if res.status_code == 404:
                await msg.answer(f"послуг немає. додати прайс послуг /add_service")
                return
            elif res.status_code != 200:
                await msg.answer(f"Ми не знайомі ((( тицяєм /start")
                return

            booking_datetime = callback_data['datetime']
            async with state.proxy() as data:
                data['booking_datetime'] = booking_datetime

            await msg.answer(f"введить ім'я кліента чи номер телефона чи введить . щоб припинити")

            await CreateBookingFSM.start.set()
            await CreateBookingFSM.next()


@dp.message_handler(state=CreateBookingFSM.clients)
async def show_clients(msg: Message, state: FSMContext):

    await state.update_data({'search_data': msg.text})

    if msg.text == '.':
        await state.finish()
        await msg.answer("🥷 зірвався з крючка")
        return
    filters = {'search_data': msg.text}
    await msg.answer(f"😺 оберіть кліента, чи додайте", reply_markup=await clients_keyboard(filters, msg))

    await CreateBookingFSM.client.set()


@dp.callback_query_handler(client_common_callback.filter(), state=CreateBookingFSM.client)
async def client_add_cancel(callback_query: CallbackQuery, callback_data: CallbackData):
    msg = callback_query.message
    act = callback_data['act']

    match act:
        case 'CANCEL':
            await msg.delete_reply_markup()
            await msg.answer("🥷 зірвався з крючка")

        case 'ADD_CLIENT':
            await msg.delete_reply_markup()
            await CreateBookingFSM.clients.set()
            await CreateBookingFSM.next()
            await msg.answer(f"додаєм кліента, введить ім'я чи введить . щоб припинити")


@dp.message_handler(state=CreateBookingFSM.name)
async def create_client_name(msg: Message, state: FSMContext):

    await state.update_data({'name': msg.text})

    if msg.text == '.':
        await state.finish()
        await msg.answer("🥷 зірвався з крючка")
        return

    await CreateBookingFSM.next()
    await msg.answer(f"номер телефона")


def validate_phone(user_id: int, phone_number: str):
    res = ResponseAPI('/api/phone_number/', user_id).get({'phone_number': phone_number})

    return res.status_code == 200


@dp.message_handler(state=CreateBookingFSM.phone_number)
async def create_client(msg: Message, state: FSMContext):

    await state.update_data({'phone_number': msg.text})
    data = await state.get_data()

    if msg.text == '.':
        await state.finish()
        await msg.answer("🥷 зірвався з крючка")
        return

    if phone_number := validate_phone(msg.from_id, data['phone_number']):
        client = ClientCreate(user_id=msg.from_id, name=data['name'], phone_number=phone_number)
        res = ResponseAPI('/api/clients/', msg.from_id).post(client)
        client = Client(**res.json())

        await state.update_data({'client_id': client.id})

        await msg.answer(str(client))

        await CreateBookingFSM.next()

        await msg.answer(f"оберіть послугу", reply_markup=await booking_service_keyboard(msg.from_id))
    else:
        await msg.answer(f"недостатньо цифр")
        await CreateBookingFSM.phone_number.set()


@dp.callback_query_handler(booking_client_callback.filter(), state=CreateBookingFSM.client)
async def create_booking_client(callback_query: CallbackQuery, callback_data: CallbackData, state: FSMContext):
    msg = callback_query.message
    await msg.delete_reply_markup()

    await state.update_data({'client_id': callback_data['client_id']})

    await CreateBookingFSM.next()
    await msg.answer(f"оберіть послугу", reply_markup=await booking_service_keyboard(msg.chat.id))


@dp.callback_query_handler(booking_service_callback.filter(), state=CreateBookingFSM.service)
async def create_booking_service(callback_query: CallbackQuery, callback_data: CallbackData, state: FSMContext):
    msg = callback_query.message
    await msg.delete_reply_markup()

    await state.update_data({'service_id': callback_data['service_id']})

    await CreateBookingFSM.next()
    await msg.answer(f"коммент чи введить . щоб пропустити")


@dp.message_handler(state=CreateBookingFSM.comment)
async def create_booking_comment(msg: Message, state: FSMContext):

    comment = "" if msg.text == "." else msg.text
    await state.update_data({'comment': comment})
    data = await state.get_data()
    # data = data.as_dict()

    booking = BookingCreate(
        user_id=msg.chat.id,
        start_datetime=data['booking_datetime'],
        client_id=data['client_id'],
        service_id=data['service_id'],
        comment=data['comment']
    )
    res = ResponseAPI('/api/bookings/', msg.chat.id).post(booking)

    booking = ServiceBooking(**res.json())

    current_date = booking.start_datetime.date()

    await state.finish()

    await msg.answer(f"запис {booking}\n"
                     f"🗓 Дивимось наш графік 🗓",
                     reply_markup=await master_date_keyboard(msg.chat.id, current_date))

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.types import CallbackQuery
from aiogram.utils.callback_data import CallbackData

from ..bot import dp
from ..keyboards.times import times_keyboard, time_callback
from ..schemas.schedule import ScheduleCreate, Schedule
from ..state.schedule import CreateScheduleFSM
from ..utils.response_api import ResponseAPI


@dp.message_handler(Command("add_schedule"))
async def create_schedule_start(msg: types.Message):
    res = ResponseAPI('/api/schedules/', msg.from_id).get()

    if res.status_code == 404:
        await msg.answer(f"зараз створюемо график за яким ви працюете, графік може бути тільки один")
        await CreateScheduleFSM.start.set()
        await CreateScheduleFSM.next()
        await msg.answer(f"оберіть початок роботі", reply_markup=await times_keyboard())
        return
    elif res.status_code != 200:
        await msg.answer(f"Ми не знайомі ((( тицяєм /start")
        return

    schedules = [Schedule(**i) for i in res.json()]
    await msg.answer(f"Ваш графік {schedules[0].name}")


@dp.callback_query_handler(time_callback.filter(), state=CreateScheduleFSM.start_time)
async def create_schedule_start_time(callback_query: CallbackQuery, callback_data: CallbackData, state: FSMContext):
    msg = callback_query.message
    await msg.delete_reply_markup()

    await state.update_data({'start_time': callback_data['time']})

    await msg.answer(f"починаю працювати о {callback_data['time']}")
    await CreateScheduleFSM.next()
    await msg.answer(f"коли закінчую працювати?", reply_markup=await times_keyboard())


@dp.callback_query_handler(time_callback.filter(), state=CreateScheduleFSM.end_time)
async def create_schedule_end_time(callback_query: CallbackQuery, callback_data: CallbackData, state: FSMContext):
    msg = callback_query.message

    await state.update_data({'end_time': callback_data['time']})
    data = await state.get_data()

    await msg.delete_reply_markup()
    await msg.answer(f"закінчую працювати о {data['end_time']}")

    schedule = ScheduleCreate(user_id=msg.chat.id, start_time=data['start_time'], end_time=data['end_time'])

    res = ResponseAPI('/api/schedules/', msg.chat.id).post(schedule)
    schedule = Schedule(**res.json())

    await state.finish()
    await msg.answer(f"Ваша графік {schedule.name}")

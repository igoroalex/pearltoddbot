from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.types import Message

from ..bot import dp
from ..schemas.workplace import WorkplaceCreate, Workplace
from ..state.workplace import CreateWorkplaceFSM
from ..utils.response_api import ResponseAPI


@dp.message_handler(Command("add_address"))
async def create_workplace_start(msg: Message):
    res = ResponseAPI('/api/workplaces/', msg.from_id).get()

    if res.status_code == 404:
        await msg.answer(f"введіть адресу, наприклад: Бьюти салон на Космонавтов 15")
        await CreateWorkplaceFSM.start.set()
        await CreateWorkplaceFSM.next()

    workplaces = [Workplace(**i) for i in res.json()]
    await msg.answer(f"вже є адреса {workplaces[0].address}")


@dp.message_handler(state=CreateWorkplaceFSM.address)
async def create_workplace_address(msg: Message, state: FSMContext):

    await state.update_data({'address_workplace': msg.text})
    data = await state.get_data()

    workplace = WorkplaceCreate(user_id=msg.from_id, address=data['address_workplace'])

    res = ResponseAPI('/api/workplaces/', msg.from_id).post(workplace)
    workplace = Workplace(**res.json())

    await state.finish()
    await msg.answer(f"Ваша адреса {workplace.address}")

from .bot import dp, bot
from .utils.set_bot_commands import set_commands

import app.handlers
import app.utils.logging


async def main():

    await set_commands()

    await dp.skip_updates()

    try:
        await dp.start_polling()
    finally:
        await dp.storage.close()
        await dp.storage.wait_closed()
        await bot.get_session().close()

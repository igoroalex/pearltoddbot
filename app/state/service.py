from aiogram.dispatcher.filters.state import StatesGroup, State


class CreateServiceFSM(StatesGroup):
    service = State()
    start = State()
    name = State()
    price = State()
    duration = State()
    rest_time = State()
    description = State()
    comment = State()

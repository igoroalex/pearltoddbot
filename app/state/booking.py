from aiogram.dispatcher.filters.state import StatesGroup, State


class CreateBookingFSM(StatesGroup):
    start = State()
    clients = State()
    name = State()
    phone_number = State()
    client = State()
    service = State()
    comment = State()

from aiogram.dispatcher.filters.state import StatesGroup, State


class CreateScheduleFSM(StatesGroup):
    start = State()
    start_time = State()
    end_time = State()

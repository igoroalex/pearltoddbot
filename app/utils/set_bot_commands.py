from aiogram.types import BotCommand

from app.bot import bot


async def set_commands():
    commands = [
        BotCommand(command='/help', description="допомога"),
        BotCommand(command='/add_work_date', description="додати розклад по дням"),
        BotCommand(command='/add_service', description="додати прайс послуг"),
        BotCommand(command='/show_master_calendar', description="показати записи"),
        BotCommand(command='/add_schedule', description="додати графік роботи"),
        BotCommand(command='/add_address', description="додати адресу"),
    ]
    await bot.set_my_commands(commands)

import requests

from app.schemas.token import Token
from config import settings


class ResponseAPI:
    def __init__(self, path: str, user_id: int):
        self.url = self._get_url(path)
        self.user_id = user_id
        self.token = self._get_token()
        self.headers = {"Authorization": f"Bearer {self.token}"}

    def get(self, filters=None):
        params = {"user_id": self.user_id}
        if filters:
            params.update(filters)
        return requests.get(self.url, params=params, headers=self.headers)

    def post(self, data):
        return requests.post(self.url, data=data.json(), headers=self.headers)

    def patch(self, data):
        return requests.patch(self.url, data=data.json(), headers=self.headers)

    def delete(self, data):
        return requests.delete(self.url, data=data.json(), headers=self.headers)

    @staticmethod
    def _get_url(path: str):
        return f"{settings.URL_API}{path}"

    def _get_token(self):
        token = Token(user_id=self.user_id)
        return token.access_token

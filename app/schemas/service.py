from pydantic import BaseModel


class ServiceBase(BaseModel):
    name: str
    price: int
    duration: int
    rest_time: int
    description: str = ''
    comment: str = ''

    def __str__(self):
        return (
            f"{self.name} - {self.price}\n"
            f"duration {self.duration}, rest {self.rest_time}\n"
            f"desc {self.description}\n"
            f"comment {self.comment}\n"
        )

    def short_name(self):
        return f"{self.name} - {self.price}"


class ServiceCreate(ServiceBase):
    user_id: int


class Service(ServiceBase):
    id: int

from pydantic import BaseModel


class ClientBase(BaseModel):
    name: str = ''
    phone_number: str = ''


class ClientCreate(ClientBase):
    user_id: int


class Client(ClientBase):
    id: int

    def __str__(self):
        return f"{self.name} {self.phone_number}"

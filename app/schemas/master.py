from pydantic import BaseModel


class MasterCreate(BaseModel):
    full_name: str
    telegram_id: int
    username: str

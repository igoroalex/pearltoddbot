from pydantic import BaseModel


class WorkplaceCreate(BaseModel):
    user_id: int
    address: str


class Workplace(BaseModel):
    address: str

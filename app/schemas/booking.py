from datetime import datetime

from pydantic import BaseModel


class BookingCreate(BaseModel):
    user_id: int = None
    client_id: int
    start_datetime: datetime
    service_id: int
    comment: str = ''


class ZeroBooking(BaseModel):
    start_datetime: datetime
    zero: bool = True


class ServiceBooking(BaseModel):
    id: int
    service_name: str
    client_name: str
    start_datetime: datetime
    end_datetime: datetime
    start_end_datetime: str = ''
    service_comment: str = ''
    zero: bool = False
    comment: str = ''

    def __str__(self):
        return (
            f"⏰ {self.start_end_datetime}\n"
            f"🪄 {self.service_name}\n"
            f"😀 {self.client_name}\n"
            f"🗒 {self.service_comment}\n"
            f"🗒 {self.comment}"
        )

    def master_date(self):
        return f"⏰ {self.start_end_datetime} 🪄 {self.service_name} 😀 {self.client_name}"

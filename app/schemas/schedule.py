from datetime import time

from pydantic import BaseModel


class ScheduleBase(BaseModel):
    start_time: time
    end_time: time


class ScheduleCreate(ScheduleBase):
    user_id: int


class Schedule(ScheduleBase):
    id: int
    name: str

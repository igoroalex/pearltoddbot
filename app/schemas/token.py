from datetime import datetime, timezone
from typing import List

from jose import jwt
from pydantic import BaseModel, root_validator

from config import settings


class Token(BaseModel):
    user_id: int
    access_token: str = ''
    scope: List[str] = ['master']
    date_time: datetime = None

    @root_validator
    def get_service_name(cls, values):
        to_encode = dict()
        to_encode.update({"user_id": values['user_id']})
        to_encode.update({"scopes": values['scope']})
        to_encode.update({"date_time": datetime.now(tz=timezone.utc).isoformat()})
        values['access_token'] = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
        return values

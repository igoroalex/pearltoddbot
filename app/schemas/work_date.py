from datetime import date

from pydantic import BaseModel


class WorkDateCreate(BaseModel):
    user_id: str
    date: date


class WorkDate(BaseModel):
    date: date
